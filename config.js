require.config({
  shim: {

  },
  paths: {
    angular: "app/vendors/angular/angular",
    "angular-bootstrap": "app/vendors/angular-bootstrap/ui-bootstrap-tpls",
    "angular-ui-router": "app/vendors/angular-ui-router/release/angular-ui-router",
    "bootstrap-sass": "app/vendors/bootstrap-sass/assets/javascripts/bootstrap",
    jquery: "app/vendors/jquery/dist/jquery",
    "angular-mocks": "app/vendors/angular-mocks/angular-mocks"
  },
  packages: [

  ]
});
