# __ANGULAR-BOOTSTRAP-GRUNT-BOILERPLATE__

>This repo contains a simplified boilerplate to start a project with __grunt__, __angular__ and __bootstrap__ in order to make easier and faster the template's developing

##### PACKAGE.JSON

[![Dependency Status](https://www.versioneye.com/user/projects/56b8832ff6e5060033d602ce/badge.svg?style=flat)](https://www.versioneye.com/user/projects/56b8832ff6e5060033d602ce)


#### BOWER.JSON

[![Dependency Status](https://www.versioneye.com/user/projects/56b8832ef6e506003a88f247/badge.svg?style=flat)](https://www.versioneye.com/user/projects/56b8832ef6e506003a88f247)


Developed following the best practice for Angular.js (https://github.com/johnpapa/angular-styleguide), in anticipation of the arrival of Angular 2

---

## __SET UP__

Before cloning the repo **be sure** you have installed:

* [NodeJs & npm](http://nodejs.org/download/) (version >= 4.2.2)
* [Grunt](http://gruntjs.com/getting-started) (latest version)
* [Bower](http://bower.io/) (latest version)
* [Sass](http://sass-lang.com/install) (latest version)


- Choose a folder project in your system and switch in `cd [folder path]`
- Clone the repo in your folder `git clone https://ibbatta@bitbucket.org/ibbatta/my-boilerplate.git`

---

## __INSTALLATION__

To install the npm repositories and bower packages run (from the directory of the project): `npm install && bower install`

---

## __USAGE__

Once everything is installed, use grunt from the terminal to start the build tasks.
The Gruntfile expose these tasks:

- `grunt server` (start the project locally)
- `grunt dist` (minify js, html and css files)
- `grunt clean` (clean / remove tmp, dist and bower's component folders)

---

## __CONTRIBUTING__

- Fork it!
- Create your feature branch: `git checkout -b my-new-feature`
- Commit your changes: `git commit -am 'Add some feature'`
- Push to the branch: `git push origin my-new-feature`
- Submit a pull request

---

## __HISTORY__

TODO:

---

## __CREDITS__

- [Maurizio Battaghini](https://github.com/ibbatta)
- [Aristeidis Bampakos](https://github.com/bampakoa)

---

## __TODO__

- _Prepare images for **dist**_
- _Automate **changelog** updates_

---

### __TROUBLESHOOTING__ ###

This boilerplate is not ready for production yet

---

## __LICENSE__

[![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)](http://creativecommons.org/publicdomain/zero/1.0/)

[Full license](LICENSE)
